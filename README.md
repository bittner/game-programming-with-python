Programming with pygame
=======================

A simple program showing off some of pygame's capabilities.

Resources
---------

- [Pygame > Getting Started](https://www.pygame.org/wiki/GettingStarted)
- [Pygame API documentation](https://www.pygame.org/docs/)
- [Game development using Python](https://www.edureka.co/blog/pygame-tutorial) (blog)
- [Tutorial videos](https://www.youtube.com/results?search_query=pygame+tutorial+video) (YouTube)

### Examples

- [Pygame > examples](https://www.pygame.org/docs/ref/examples.html)
  ([source code](https://github.com/pygame/pygame/tree/master/examples))
- [Pygame > Solarwolf](https://github.com/pygame/solarwolf)
- [Pygame > Parrotjoy](https://github.com/pygame/parrotjoy)
- [Pygame > Stuntcat](https://github.com/pygame/stuntcat)
