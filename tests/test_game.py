"""
Tests for game module
"""
from unittest.mock import patch

import game


def terminate_game():
    from game import Game
    Game.over = True


@patch("game.shutdown")
@patch("game.update_screen")
@patch("game.process_events", side_effect=terminate_game)
@patch("game.init_game")
def test_run(mock_init_game, mock_process_events, mock_update_screen, mock_shutdown):
    game.run()
    assert mock_init_game.called
    assert mock_process_events.called
    assert mock_update_screen.called
    assert mock_shutdown.called


@patch("pygame.display.set_mode")
@patch("pygame.init")
def test_init_game(mock_pygame_init, mock_pygame_display_setmode):
    game.init_game()
    assert mock_pygame_init.called
    assert mock_pygame_display_setmode.assert_called


@patch("pygame.quit")
def test_shutdown(mock_pygame_quit):
    game.shutdown()
    assert mock_pygame_quit.called


@patch("pygame.event.get")
def test_process_events(mock_gygame_event):
    game.process_events()
    assert mock_gygame_event.called


@patch("pygame.display.flip")
def test_update_screen(mock_pygame_disply_flip):
    game.update_screen()
    assert mock_pygame_disply_flip.called
