"""
Showing off pygame's capabilities
"""
import pygame


class Game:
    over = False


def run():
    init_game()

    while not Game.over:
        process_events()
        update_screen()

    shutdown()


def init_game():
    pygame.init()
    pygame.display.set_mode(size=(800, 600))


def shutdown():
    pygame.quit()


def process_events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            Game.over = True


def update_screen():
    pygame.display.flip()


if __name__ == "__main__":
    run()
